# Basic api

    npm init

    npm install -g express-generator

    npm install express --save

The save flag is used to edit your package.json file and add express as a dependency.

Creating the app.

Then create the app.js file or whatever you prefer naming it (default is index.js) and add in the following code.

    var express = require("express");
    var app = express();

    app.listen(3000, () => {
    console.log("Server running on port 3000");
    });

Congrats! You just made your first useless express server!

    node app.js

Your app will now be accessible using `http://localhost:3000`

    app.get("/url", (req, res, next) => {
    res.json(["Tony","Lisa","Michael","Ginger","Food"]);
    });

    node app.js

open up your browser and enter `http://localhost:3000/url`

# HTTP Methods

![Image](<HTTP methods.png>)

# Another Node API

Create data.js file. Feel free to add items, and item details.

        // data.js
        module.exports = [
            { "id": "1234", "name": "The Lion King", "genre": "animation"},
            { "id": "5678", "name": "The Godfather", "genre": "crime"},
            { "id": "9101", "name": "The Dark Knight", "genre": "action"},
        ];

module.exports exposes this array to other files

        // app.js
        let data = require('./data');

# GET

Get all items
  
GET /items

---

        app.get("/items", (req, res) => {
        res.json(data);
        });

Get one item identified by :id
GET /items/:id

We use Array.find to get the first item that matches the condition,
then display a message if not found.

Note that the item id is a string e.g. 1234, so we can compare it using=== directly.
You'll have to parse before comparing, if you are using an integer id.

        app.get("/items/:id", (req, res) => {
        const itemId = req.params.id;
        const item = data.find(_item => _item.id === itemId);

        if (item) {
            res.json(item);
        } else {
            res.json({ message: `item ${itemId} doesn't exist`})
        }
        });

---

        $ curl http://localhost:4000/items

---

## Result

        [{"id":"1234","name":"The Lion King","genre":"animation"},{"id":"5678","name":"The Godfather","genre":"crime"},{"id":"9101","name":"The Dark Knight","genre":"action"}]

---

        $ curl http://localhost:4000/items/1234

---

## Result

        {"id":"1234","name":"The Lion King","genre":"animation"}

---

        $ curl http://localhost:4000/items/blahblah

## Result

        {"message":"item blahblah doesn't exist"}

# POST

To encode the body of the request sent by client in a POST message, we need body-parser middleware.
This allows us to use req.body in our route handler

        npm install body-parser

Then we import and use it. We'll just accept JSON-encoded body for now.

    // app.js

    const body_parser = require('body-parser');

    // parse JSON (application/json content-type)
    app.use(body_parser.json());

# Post an item

    POST /items

Here, we are getting entire item from req.body since it matches our data,
but note that it is also possible to just get, e.g. req.body.name

        // app.js

        ...
        app.post("/items", (req, res) => {
        const item = req.body;
        console.log('Adding new item: ', item);

        // add new item to array
        data.push(item)

        // return updated list
        res.json(data);
        });

---

        $ curl -X POST -H "Content-Type: application/json" --data '{"id": "4568", "name": "Forrest Gump", "genre": "drama"}' http://localhost:4000/items

---

## Result

        [..., {"id":"4568","nam4568e":"Forrest Gump","genre":"drama"}]

# PUT

To update an item, we expect client to pass item id in the URL param (req.params.id)
and the updated object in the body (req.body)

Here we simply replace the old one using forEach, but you could apply your own
algorithm to replace an object or an object's attribute in an array of objects.

        // update an item
        app.put("/items/:id", (req, res) => {
        const itemId = req.params.id;
        const item = req.body;
        console.log("Editing item: ", itemId, " to be ", item);

        const updatedListItems = [];
        // loop through list to find and replace one item
        data.forEach(oldItem => {
            if (oldItem.id === itemId) {
                updatedListItems.push(item);
            } else {
                updatedListItems.push(oldItem);
            }
        });

        // replace old list with new one
        data = updatedListItems;

        res.json(data);
        });

Let's say you really think The Dark Knight is a drama 😢 instead of action,...

        $ curl -X PUT -H "Content-Type: application/json" --data '{"id": "9101", "name": "The Dark Knight", "genre": "drama"}' http://localhost:4000/items/9101

---

        ...{"id":"9101","name":"The Dark Knight","genre":"drama"}...

# DELETE

Lastly for delete, we only need id URL param from client.
We filter the array, excluding the item to be deleted.

        // delete item from list
        app.delete("/items/:id", (req, res) => {
        const itemId = req.params.id;

        console.log("Delete item with id: ", itemId);

        // filter list copy, by excluding item to delete
        const filtered_list = data.filter(item => item.id !== itemId);

        // replace old list with new one
        data = filtered_list;

        res.json(data);
        });

---

        $ curl -X DELETE http://localhost:4000/items/1234

---

## Result

        [{"id":"1234","name":"The Lion King","genre":"animation"},{"id":"tt0068646","name":"The Godfather","genre":"crime"}]

Usefull commands:

POST Command

                curl -X POST -H "Content-Type: application/json"  --data '{ "name":"apples", "price":500}' http://localhost:3000/

GET Command

                curl -X GET http://localhost:3000/all

PUT Command

                curl -X PUT -H "Content-Type: application/json"  --data '{"id":"5e32afebd18f8f316d8c3f7c", "name":"tomato", "price":800}' http://localhost:3000/update/record

DELETE Command

                curl -X DELETE -H "Content-Type: application/json"  --data '{ "id":"5e32afebd18f8f31"}' http://localhost:3000/

---

                $ mongo

                > show dbs

                > use test

                > show collections

                > db.products.find()

                >db.products.find().pretty()

---
